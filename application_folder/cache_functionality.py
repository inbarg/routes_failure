import itertools


class CacheFunctionality:

    # {ID : counter}
    entry_search_counter: dict

    # {ID : entry}, contains a searched-counter for each ID
    top_searched_entries: dict

    # [ int ], list of sorted ids according to their counter
    sorted_ids: list

    cache_size: int

    def __init__(self, dict_size, table_object):
        self.entry_search_counter = {}
        self.top_searched_entries = {}
        self.sorted_ids = []
        self.cache_size = dict_size
        self.table_object = table_object

    def remove_entry(self, _id):
        """
        Remove entry from cache.
        """
        self.top_searched_entries.pop(_id)

    def add_entry(self, _id):
        """
        Adds entry to cache
        """
        entry = self.table_object.query.get(_id)
        self.top_searched_entries[_id] = entry

    def get_entry(self, _id):
        """
        returns entry from cache
        """
        self.update_cache(_id)

        if self.check_entry(_id):
            return self.top_searched_entries[_id]
        else:
            return False

    def check_entry(self, _id):
        """
        Checks if entry exist in cache
        """
        print(self.top_searched_entries)
        return True if _id in self.top_searched_entries.keys() else False

    def update_cache(self, _id):
        """
        Adds hit for entry, update cache
        """
        if _id in self.entry_search_counter.keys():
            self.entry_search_counter[_id] += 1
        else:
            self.entry_search_counter[_id] = 1

        self.sorted_ids.clear()

        # Sort entry_search_counter dictionary, output to sorted ids list
        for _id in sorted(self.entry_search_counter.items(), key=lambda item: item[1], reverse=True):
            self.sorted_ids.append(_id[0])

        # While cache is not full yet
        if len(self.top_searched_entries) < self.cache_size:
            for _id in self.sorted_ids:

                # If entry doesn't exist in top list - add it
                if _id not in self.top_searched_entries.keys():
                    self.add_entry(_id)

            return

        # If no changes in the cache after updating - return.
        if self.sorted_ids[0:self.cache_size] == self.top_searched_entries.keys():
            return

        # Remove entry that is no longer in the top x search
        for _id in list(self.top_searched_entries.keys()):
            if _id not in self.sorted_ids[0:self.cache_size]:
                self.remove_entry(_id)
                break

        # Find the entry to add to the top search dict and add it.
        for _id in itertools.islice(self.sorted_ids, self.cache_size):
            if _id[0] not in self.top_searched_entries.keys():
                self.add_entry(_id[0])

        return
