class ProfileAlreadyExistsException(Exception):
    pass


class ProfileDoesntExistException(Exception):
    pass


class UsernameDoesntExistException(Exception):
    pass


class UserAlreadyExistsException(Exception):
    pass
