from application_folder.linkedin_profile import LinkedInProfile
from application_folder.db_functionality import DbFunctionality
from application_folder.db import db
import unittest


class UnitTests(unittest.TestCase):

    def test_basic_profile_entry(self):
        raw_profile = {"Id number": "1", "First name": "John", "Last name": "Daw", "Company": "aidoc", "One liner": ""}
        profile = LinkedInProfile.query.get('1')
        assert profile["Id number"] == raw_profile["Id number"]
        assert profile["First name"] == raw_profile["First name"]
        assert profile["Last name"] == raw_profile["Last name"]
        assert profile["Company"] == raw_profile["Company"]

    def test_add_entry(self):
        mock_profile = LinkedInProfile(id="10", first_name="John", last_name="Daw", company="aidoc", one_liner="")
        db_obj = DbFunctionality(db)
        db_entry = db_obj.add_entry(mock_profile)
        assert db_entry["Id number"] == mock_profile["Id number"]
        assert db_entry["First name"] == mock_profile["First name"]
        assert db_entry["Last name"] == mock_profile["Last name"]
        assert db_entry["Company"] == mock_profile["Company"]


if __name__ == "__main__":
    unittest.main()