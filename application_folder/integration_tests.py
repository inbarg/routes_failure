from application_folder import create_app
import unittest
from application_folder.cache_functionality import CacheFunctionality
from application_folder.linkedin_profile import LinkedInProfile
from application_folder.db_functionality import DbFunctionality
from application_folder.db import db


class IntegrationTests(unittest.TestCase):

    def setUp(self) -> None:
        self.app = create_app(configuration="testing")
        self.app = self.app.test_client()

    def test_cache_app_integration(self):
        self.app.post('/login', json={"username": "admin", "password": "admin"})
        profile = self.app.get('/profiles/1')
        cache_obj = CacheFunctionality(1, LinkedInProfile)
        cache_entries = cache_obj.get_entry(1)
        assert cache_entries[1] == profile

    def test_db_app_integration(self):
        self.app.post('/login', json={"username": "admin", "password": "admin"})
        profile = self.app.post('/profiles/', json={"Id number": "10", "First name": "John",
                                                    "Last name": "Daw", "Company": "aidoc",
                                                    "One liner": ""})
        db_obj = DbFunctionality(db)
        db_entry = db_obj.get_profile("10")
        db_entry.pop("creation date")
        assert db_entry == profile


if __name__ == "__main__":
    unittest.main()
