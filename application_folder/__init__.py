import json
from flask import Flask
from application_folder.db import db
from application_folder.linkedin_cache import LinkedinCache
from application_folder.linkedin_profile import LinkedInProfile


def create_app(configuration='flask'):

    app = Flask(__name__)

    with open('config.json') as file:
        config = json.load(file)

    with app.app_context():
        app.config.from_mapping(config[configuration])

        _db_functions = LinkedinCache(db, config['cache']['CACHE_SIZE'], LinkedInProfile)
        db.init_app(app)
        db.create_all()

        from . import routes
        routes.inject(_db_functions)

        return app
