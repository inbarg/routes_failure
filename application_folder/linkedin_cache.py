from application_folder.db_functionality import DbFunctionality
from application_folder.cache_functionality import CacheFunctionality
from application_folder.linkedin_profile import LinkedInProfile


class LinkedinCache(DbFunctionality):

    def __init__(self, db, dict_size, table_object):
        DbFunctionality.__init__(self, db)
        self.cache_obj = CacheFunctionality(dict_size, table_object)

    def get_profile(self, _id):
        """
        Overrides super function in order to retrieve information from cache if possible.
        """

        result = self.cache_obj.get_entry(_id)

        if result:
            return LinkedInProfile.serialize(result)
        else:
            return super().get_profile(_id)
