from application_folder.db import db
from sqlalchemy.sql import func


class LinkedInProfile(db.Model):

    __tablename__ = "profiles"
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    company = db.Column(db.String)
    one_liner = db.Column(db.String(200))
    creation_date = db.Column(db.DateTime, server_default=func.now())

    def serialize(self):
        return {
            'id': self.id,
            'first name': self.first_name,
            'last name': self.last_name,
            'company': self.company,
            'one liner': self.one_liner,
            'creation date': self.creation_date
        }
