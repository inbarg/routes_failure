from application_folder.db import db


class SystemUser(db.Model):

    __tablename__ = "users"
    username = db.Column(db.String, primary_key=True, nullable=False)
    password = db.Column(db.String, nullable=False)
    authorized = db.Column(db.Boolean, nullable=True, default=False)

    def serialize(self):
        return {
            'username': self.username,
            'password': self.password,
            'authorized': self.authorized
        }
