from flask import current_app as app
from application_folder import db_exceptions as exc
from application_folder.db_functionality import DbFunctionality
from application_folder.system_user import SystemUser
from application_folder.linkedin_profile import LinkedInProfile
from functools import wraps
from flask import request, session, jsonify, Response

_db_functions: DbFunctionality


def inject(db_functions):
    global _db_functions
    _db_functions = db_functions


def exclude_from_active_session(func):
    """
    Excludes routes for before-request decorators
    """
    func._exclude_from_active_session = True
    return func


def require_authorization(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        if session:
            if session['user']['authorized']:
                return func(*args, *kwargs)
            else:
                return Response("Unauthorized action.", status=401)

    return wrap


@app.before_request
def before_request():
    if request.endpoint in app.view_functions:
        view_func = app.view_functions[request.endpoint]
        if not hasattr(view_func, '_exclude_from_active_session'):
            if 'user' not in session:
                return Response('User not logged in.', status=405)


@app.route('/login', methods=['POST'])
@exclude_from_active_session
def login():
    username = request.json['username']
    password = request.json['password']

    try:
        user = _db_functions.get_user(username)
        if user['password'] == password:
            session['user'] = user
            return Response("User {0} is logged in.".format(username), status=200)
        else:
            return Response("Incorrect password", status=401)

    except exc.UsernameDoesntExistException:
        return Response("User doesn't exist.", status=404)


@app.route('/register', methods=['POST'])
@exclude_from_active_session
def add_user():
    username = request.json['username']
    password = request.json['password']

    if username != 'admin':
        user = SystemUser(username=username, password=password, authorized=False)
    else:
        user = SystemUser(username=username, password=password, authorized=True)
    try:
        _db_functions.add_entry(user)
        session['user'] = username
        session['password'] = password

    except exc.UserAlreadyExistsException:
        return Response("User already exist in DB.", status=401)

    return jsonify(user.serialize())


@app.route('/profiles', methods=['POST'])
def add_profile():
    """
    Adds profile to server
    :return: profile object
    """
    id_number = request.json['Id number']
    first_name = request.json['First name']
    last_name = request.json['Last name']
    company = request.json['Company']
    one_liner = request.json['One liner']

    profile = LinkedInProfile(id=id_number, first_name=first_name, last_name=last_name, company=company,
                              one_liner=one_liner)

    try:
        _db_functions.add_entry(profile)
    except exc.ProfileAlreadyExistsException:
        return Response("Profile already exist in DB.", status=401)

    parsed_profile = LinkedInProfile.serialize(profile)
    return jsonify(parsed_profile)


@app.route('/profiles/<user_id>', methods=['GET'])
def get_profile(user_id):
    """
    Gets specific profile
    :return: a profile
    """
    try:
        return jsonify(_db_functions.get_profile(user_id))

    except exc.ProfileDoesntExistException:
        return Response('Profile doesnt exist', status=404)


@app.route('/profiles/<user_id>', methods=['DELETE'])
@require_authorization
def remove_profile(user_id):
    """
    Removes profile according to its id number.
    ** Authorized users only
    :return:
    """
    try:
        _db_functions.remove_entry(user_id)
        return Response('Removed db entry.', status=204)

    except exc.ProfileDoesntExistException:
        return Response('Profile doesnt exist.', status=404)


@app.route('/sign_out', methods=['GET'])
def sign_out():
    session.pop('user')
    return Response('User logged out.', status=200)


if __name__ == "__main__":
    app.run()
