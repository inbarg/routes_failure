from flask import Response
from sqlalchemy import exc as exception
from application_folder.linkedin_profile import LinkedInProfile
from application_folder.system_user import SystemUser
from application_folder import db_exceptions


class DbFunctionality:

    _db = None

    def __init__(self, db):
        self._db = db

    def add_entry(self, entry):
        """
        Adds entry to DB
        :param entry: profile object
        """

        try:
            self._db.session.add(entry)
            self._db.session.commit()

        except exception.IntegrityError:
            self._db.session().rollback()
            entry_type = type(entry)
            if entry_type == LinkedInProfile:
                raise db_exceptions.ProfileAlreadyExistsException
            elif entry_type == SystemUser:
                raise db_exceptions.UserAlreadyExistsException

    def get_profile(self, id_number):
        """
        Get specific entry from DB
        :return: Entry if exist
        """
        profile = LinkedInProfile.query.get(id_number)

        if profile is None:
            raise db_exceptions.ProfileDoesntExistException

        parsed_profile = LinkedInProfile.serialize(profile)
        return parsed_profile

    def get_user(self, username):
        """
        Get specific entry from DB
        :return: Entry if exist
        """
        user = SystemUser.query.get(username)

        if user is None:
            raise db_exceptions.UsernameDoesntExistException

        parsed_user = SystemUser.serialize(user)

        return parsed_user

    def remove_entry(self, id_number):
        """
        Removes profile entry from DB according to Id number
        """
        try:
            LinkedInProfile.query.filter(LinkedInProfile.id == id_number).delete()
            self._db.session.commit()

            return Response("Entry was removed from db", status=204)

        except exception.NoReferenceError:
            raise db_exceptions.UsernameDoesntExistException
