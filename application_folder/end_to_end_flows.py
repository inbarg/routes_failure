import unittest
from application_folder import create_app


class TestEndToEndFlow(unittest.TestCase):

    def setUp(self):
        self.app = create_app(configuration='testing')
        self.app = self.app.test_client()

    def tearDown(self):
        self.app.get('/sign_out')

    # Helper functions
    def login(self, username, password):
        response = self.app.post('/login', json=(dict(username=username, password=password)))
        print('login: {0}, {1}'.format(response, username))

    def register(self, username, password):
        response = self.app.post('/register', json=(dict(username=username, password=password)))
        print('register: {0}, {1}'.format(response, username))

    def test_simple_user(self):
        self.login('test_user', 'test_password')
        response = self.app.get('/profiles/1')
        assert response.status_code == 200
        delete_response = self.app.delete('/profiles/1')
        assert delete_response.status_code == 401

    def test_admin_access(self):
        self.login('admin', 'admin')
        get_response = self.app.get('/profiles/1')
        assert get_response.status_code == 200
        response = self.app.delete('/profiles/1')
        assert response.status_code == 204

    def test_new_user(self):
        self.register('testing4', 'testing')
        self.login('testing4', 'testing')
        new_profile_dict = {"Id number": "6", "First name": "John",
                            "Last name": "Daw", "Company": "aidoc",
                            "One liner": ""}
        new_profile_response = self.app.post('/profiles', json=new_profile_dict)
        assert new_profile_response.status_code == 200


if __name__ == "__main__":
    unittest.main()
